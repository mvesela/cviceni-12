import { __addClass, __removeClass } from '../lib/utils';

export default class Cviceni {
  constructor() {
    this.$cviceni = document.querySelector('.cviceni');
    this.$header = document.querySelector('#header');
    this.$saveButtons = document.querySelectorAll('[data-save-button]');
    this.$buttonFeedbacks = document.querySelectorAll('[data-feedback-button]');
    this.nav = {
      $prev: this.$cviceni.querySelectorAll('[data-nav-button="prev"]'),
      $next: this.$cviceni.querySelectorAll('[data-nav-button="next"]'),
    };
    this.$slides = this.$cviceni.querySelectorAll('.slide');
    this.$activeSlide = this.$cviceni.querySelector('.active-slide');
    this.$formSlide = this.$cviceni.querySelector('#slide-form');
    this.$exportSlide = this.$cviceni.querySelector('#slide-export');

    this.urlParams = (new URL(document.location)).searchParams;

    this.import = HISTORYLAB.import;
    this.isDone = this.import.done === true;
    this.isExhibition = this.urlParams.get('exid') !== null;

    this.analytics = {
      time: {
        start: {
          date: 0,
          time: 0,
        },
        end: {
          date: 0,
          time: 0,
        },
        timeSpent: 0,
      },
      activity: {
        length: 0,
        steps: [],
      },
    };
  }

  header() {
    const headerHeight = this.$header.offsetHeight;
    let y0 = 0;

    const hideHeader = (event, $item) => {
      y = $item.scrollTop;

      if (y > headerHeight && y > y0) {
        __addClass(this.$header, 'is-hidden');
        y0 = y;
      } else if (y < (y0 - 200) || y <= headerHeight) {
        __removeClass(this.$header, 'is-hidden');
        y0 = y;
      }
    };

    for (let i = 0; i < this.$slides.length; i += 1) {
      this.$slides[i].removeEventListener('scroll', hideHeader);
    }

    this.$activeSlide = this.$cviceni.querySelector('.active-slide.slide');
    let y = this.$activeSlide.scrollTop;

    if (y <= headerHeight) {
      __removeClass(this.$header, 'is-hidden');
    } else {
      __addClass(this.$header, 'is-hidden');
    }

    if (this.$activeSlide) {
      this.$activeSlide.addEventListener('scroll', (event) => hideHeader(event, this.$activeSlide));
    }
  }

  static loadingScreen() {
    const $body = document.querySelector('.loading');
    const $loadingScreen = document.querySelector('#loading-screen');
    const loaded = JSON.parse($loadingScreen.getAttribute('data-loaded'));
    const $h1 = $loadingScreen.querySelector('h1');

    setTimeout(() => {
      $h1.innerText = loaded.h1;
    }, 300);

    __removeClass($body, 'loading');
  }
}
