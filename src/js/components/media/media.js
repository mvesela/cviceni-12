import { __createNewCustomEvent } from '../../lib/utils';

const mediaEvents = {
  playchange: __createNewCustomEvent('playchange'),
  timechange: __createNewCustomEvent('timechange'),
  canplaystart: __createNewCustomEvent('canplaystart'),
};

function stopPlayingVideoPlayers($players) {
  // pause all other playing media tracks
  $players.forEach(($player) => {
    const $media = $player.querySelector('audio') || $player.querySelector('video');
    if ($media && !$media.paused) {
      $media.pause();
      $player
        .querySelector('[data-player-control]')
        .setAttribute('data-player-control', 'paused');
    }
  });
}

function getSecondsMinutesFormat(seconds) {
  const duration = {
    seconds: Math.floor(seconds),
    minutes: Math.floor(seconds / 60),
  };

  duration.seconds = (duration.seconds - (duration.minutes * 60)).toString(10);

  return `${duration.minutes}:${(`0${duration.seconds}`).slice(-2)}`;
}

export {
  getSecondsMinutesFormat,
  stopPlayingVideoPlayers,
  mediaEvents,
};
